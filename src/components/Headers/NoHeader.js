/*!

=========================================================
* Argon Dashboard React - v1.2.1
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

// reactstrap components
import { 
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
  Card,
  CardBody,
  CardTitle,
  Container,
  Row,
  Col } from "reactstrap";

const NoHeader = () => {
  return (
    <>
      <div className="header bg-gradient-info pb-6 pt-8 pt-md-8">
        <Container fluid>
          <div className="header-body">
          <Form className="navbar-search navbar-search-dark form-inline">
            <FormGroup className="mb-0">
              <InputGroup className="input-group-alternative">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    SYSTEM VERSION
                  </InputGroupText>
                </InputGroupAddon>
                <Input placeholder="5.21.06" type="text" defaultValue="" id="version" />
              </InputGroup>
              <InputGroup className="input-group-alternative">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    API ID
                  </InputGroupText>
                </InputGroupAddon>
                <Input placeholder="1234" type="text" defaultValue="" id="api_id" />
              </InputGroup>
              <InputGroup className="input-group-alternative">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    USER LOGIN
                  </InputGroupText>
                </InputGroupAddon>
                <Input placeholder="1234" type="text" defaultValue="" id="user_login" />
              </InputGroup>
            </FormGroup>
          </Form>
          </div>
        </Container>
      </div>
    </>
  );
};

export default NoHeader;
