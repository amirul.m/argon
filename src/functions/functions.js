import $ from 'jquery';
  
let ajaxForm;

export const getAjax = (json) => {
    var version = $("#version").val();
    var api_id = $("#api_id").val();
    var user_login = $("#user_login").val();

    if(version == '' || api_id == '' || user_login == '' || version == undefined || api_id == undefined || user_login == undefined) {
        alert('Please enter API credentials!')
        return false;
    }

    if(json.xs_api != undefined) {
        json.action = "http://localhost:9999/ver"+version+"/system_cc3_api.php?api_id="+api_id+"&user_login="+user_login+"&sysInPage="+json.xs_api.sysInPage;

        // If want to delete
        json.action += json.xs_api.ccsForm != undefined ? '&ccsForm='+json.xs_api.ccsForm : '';
        if(json.xs_api.Insert != undefined || json.xs_api.Update != undefined || json.xs_api.Delete != undefined) {
            json.action += json.xs_api.Insert != undefined ? '&Insert='+json.xs_api.Insert : json.xs_api.Update != undefined ? '&Update='+json.xs_api.Update : json.xs_api.Delete != undefined ? '&Delete='+json.xs_api.Delete : '';
        }
        json.action += json.xs_api.db_key != undefined && json.xs_api.db_value != undefined ? '&'+json.xs_api.db_key+'='+json.xs_api.db_value : '';
    }

    ajaxForm = $.ajax({
        url: json.action ?? '',
        type: json.method ?? 'GET',
        data: json.data ?? {},
        dataType: "json",
        // beforeSend: (xhr) => { },
        complete: (responses) => {
            console.log(responses);
            responses = responses.responseJSON != undefined ? responses.responseJSON : responses.responseText;
            
            if(json.callback != undefined && typeof json.callback == "function")
                json.callback(responses);
        }
    });
}