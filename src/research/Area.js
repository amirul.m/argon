/*!

=========================================================
* Argon Dashboard React - v1.2.1
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import $ from 'jquery';
import { getAjax } from 'functions/functions.js';

import React from 'react';
import ReactDOM from 'react-dom';

// reactstrap components
import {
    Badge,
    Card,
    CardHeader,
    CardFooter,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    DropdownToggle,
    Media,
    Pagination,
    PaginationItem,
    PaginationLink,
    Progress,
    Table,
    Container,
    Row,
    Col,
    UncontrolledTooltip,
    Button,
    Modal,
    Form,
    FormGroup,
    InputGroupAddon,
    InputGroupText,
    Input,
    InputGroup,
} from "reactstrap";
// core components

import Header from "components/Headers/NoHeader.js";

class Area extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            datas : [],
            addModal: false,
        };

        this.sysInPage_list = "master_areamst_list.php";
        this.sysInPage_maint = "master_areamst_maint.php";
    }
    
    toggleModal = () => {
        this.setState({
            addModal: !this.state.addModal
        });
    };

    reloadFunction = () => {
        getAjax({
            xs_api: {
                sysInPage: this.sysInPage_list
            },
            data: {
                "s_area_code": $("#s_area_code").val(),
                "s_area_name1": $("#s_area_name1").val()
            },
            callback: (responses) => {
                this.setState({datas: []});
                this.setState({datas: responses});
            }
        });
    }

    addFunction = (e) => {
        e.preventDefault();

        var area_code = $('#input_area_code').val();
        var area_name1 = $('#input_area_name1').val();
        var area_flag = $('#input_area_flag:checked').length > 0 ? true : false;
        var area_status = $('#input_area_status:checked').length > 0 ? true : false;
        
        getAjax({
            method: 'POST',
            xs_api: {
                sysInPage: this.sysInPage_maint,
                ccsForm: 'areamst',
                Insert: 'Insert',
            },
            data: {
                area_code: area_code,
                area_name1: area_name1,
                area_flag: area_flag,
                area_status1: area_status,
            },
            callback: (responses) => {
                if(responses.Complete != undefined) {
                    let completed = responses.Complete ?? false;
                    
                    if(completed == "done") {
                        alert("Success");
                        this.reloadFunction();
                        this.toggleModal();
                    } else 
                        alert("Fail");
                } else {
                    alert(responses);
                    this.reloadFunction();
                }
            }
        });
    }

    updateFunction = (e, id) => {
        e.preventDefault();

        var area_code = $(e.target).closest('tr').find('input[name="area_code"]').val();
        var area_name1 = $(e.target).closest('tr').find('input[name="area_name1"]').val();
        var area_flag = $(e.target).closest('tr').find('input[name="area_flag"]:checked').length > 0 ? 1 : 0;
        var area_status = $(e.target).closest('tr').find('input[name="area_status"]:checked').length > 0 ? 1 : 0;
        
        getAjax({
            method: 'POST',
            xs_api: {
                sysInPage: this.sysInPage_maint,
                ccsForm: 'areamst:Edit',
                Update: 'Update',
                db_key: 'area_no',
                db_value: id,
            },
            data: {
                area_no: id,
                area_code: area_code,
                area_name1: area_name1,
                area_flag: area_flag,
                area_status1: area_status,
            },
            callback: (responses) => {
                if(responses.Complete != undefined) {
                    let completed = responses.Complete ?? false;
                    
                    if(completed == "done") {
                        alert("Success");
                        this.reloadFunction();
                    } else 
                        alert("Fail");
                } else {
                    alert(responses);
                    this.reloadFunction();
                }
            }
        });
    }

    deleteFunction = (e, id) => {
        e.preventDefault();

        getAjax({
            method: 'POST',
            xs_api: {
                sysInPage: this.sysInPage_maint,
                ccsForm: 'areamst',
                Delete: 'Delete',
                db_key: 'area_no',
                db_value: id,
            },
            callback: (responses) => {
                if(responses.Complete != undefined) {
                    let completed = responses.Complete ?? false;
                    
                    if(completed == "done") {
                        alert("Success");
                        this.reloadFunction();
                    } else 
                        alert("Fail");
                } else {
                    alert(responses);
                    this.reloadFunction();
                }
            }
        });
    }

    checkboxChecked = (value) => {
        return value == "1" ? true : false;
    }

    listItems = (datas) => {
        if(datas == undefined)
            return false;
    
        return datas.map((data) =>
            <tr>
                <th scope="row">
                    <input type="text" name="area_code" defaultValue={data.area_code}/>
                </th>
                <td>
                    <input type="text" name="area_name1" defaultValue={data.area_name1}/>
                </td>
                <td>
                    <input type="text" name="area_state" defaultValue={data.stte_code}/>
                </td>
                <td>
                    <input type="checkbox" name="area_flag" defaultValue="1" defaultChecked={this.checkboxChecked(data.area_flag)}/>
                </td>
                <td>
                    <input type="checkbox" name="area_status" defaultValue="1" defaultChecked={this.checkboxChecked(data.area_status)}/>
                </td>
                <td className="text-right">
                <UncontrolledDropdown>
                    <DropdownToggle
                    className="btn-icon-only text-light"
                    href="#pablo"
                    role="button"
                    size="sm"
                    color=""
                    onClick={(e) => e.preventDefault()}
                    >
                    <i className="fas fa-ellipsis-v" />
                    </DropdownToggle>
                    <DropdownMenu className="dropdown-menu-arrow" right>
                    <DropdownItem
                        href="#pablo"
                        onClick={(e) => this.updateFunction(e, data.area_no)}
                    >
                        Update
                    </DropdownItem>
                    <DropdownItem
                        href="#pablo"
                        onClick={(e) => this.deleteFunction(e, data.area_no)}
                    >
                        Delete
                    </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
                </td>
            </tr>
        )
    }

    render = () => {
        return (
            <>
                <Header />
                {/* Page content */}
                <Container className="mt--7" fluid>
                {/* Dark table */}
                <Row className="mt-5">
                    <div className="col">
                    <Card className="shadow">
                        <CardHeader className="border-0">
                        <span className="mb-0">Area Master</span>
                        <button className="btn btn-primary btn-sm ml-2" onClick={this.toggleModal}>Add</button>
                        </CardHeader>
                        <Container>
                            <Row>
                                <Col md="4">
                                    <FormGroup>
                                        <Input
                                        id="s_area_code"
                                        placeholder="Search Area Code"
                                        type="text"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md="4">
                                    <FormGroup>
                                        <Input
                                        id="s_area_name1"
                                        placeholder="Search Area Name"
                                        type="text"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md="4">
                                    <button className="btn btn-primary float-right" onClick={this.reloadFunction}>Search</button>
                                </Col>
                            </Row>
                        </Container>
                        <Table
                        className="align-items-center table-flush"
                        responsive
                        >
                        <thead className="thead-light">
                            <tr>
                            <th scope="col">Code</th>
                            <th scope="col">Name</th>
                            <th scope="col">State</th>
                            <th scope="col">Active</th>
                            <th scope="col">Status</th>
                            <th scope="col" />
                            </tr>
                        </thead>
                        <tbody>
                            {this.listItems(this.state.datas)}
                        </tbody>
                        </Table>
                    </Card>
                    </div>
                </Row>
                </Container>
                {/* Modal */}
                <Modal
                    className="modal-dialog-centered"
                    isOpen={this.state.addModal}
                    toggle={() => this.toggleModal()}
                    >
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">
                        Add New Area
                        </h5>
                        <button
                        aria-label="Close"
                        className="close"
                        data-dismiss="modal"
                        type="button"
                        onClick={() => this.toggleModal()}
                        >
                        <span aria-hidden={true}>×</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <Form onSubmit={(e) => e.preventDefault()}>
                            <Row>
                                <Col md="6">
                                    <FormGroup>
                                        <Input
                                        id="input_area_code"
                                        placeholder="Area Code"
                                        type="text"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md="6">
                                    <FormGroup>
                                        <Input
                                        id="input_area_name1"
                                        placeholder="Area Name1"
                                        type="text"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md="6">
                                    <div className="custom-control custom-checkbox mb-3">
                                    <input
                                        className="custom-control-input"
                                        id="input_area_flag"
                                        type="checkbox"
                                        defaultValue="1"
                                    />
                                    <label className="custom-control-label" htmlFor="input_area_flag">
                                        Active
                                    </label>
                                    </div>
                                </Col>
                                <Col md="6">
                                    <div className="custom-control custom-checkbox mb-3">
                                    <input
                                        className="custom-control-input"
                                        id="input_area_status"
                                        type="checkbox"
                                        defaultValue="1"
                                    />
                                    <label className="custom-control-label" htmlFor="input_area_status">
                                        Status
                                    </label>
                                    </div>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                    <div className="modal-footer">
                        <Button
                        color="secondary"
                        data-dismiss="modal"
                        type="button"
                        onClick={() => this.toggleModal()}
                        >
                        Close
                        </Button>
                        <Button color="primary" type="button"
                        onClick={(e) => this.addFunction(e)}>
                        Add
                        </Button>
                    </div>
                </Modal>
            </>
        )
    }
};

export default Area;